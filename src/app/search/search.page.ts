import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Genre } from '../entities/Genre';
import { Type } from '../entities/Type';
import { ImdbSearchService } from '../services/imdb-search/imdb-search.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss'],
})
export class SearchPage {

  types: Observable<Type[]>;
  genres: Observable<Genre[]>;
  ratings: number[];

  constructor(public imdbSearchService: ImdbSearchService) {}

  ngOnInit() {
    this.types = this.imdbSearchService.getTypes();
    this.genres = this.imdbSearchService.getGenres();
    this.ratings = Array(9).fill(1).map((x, i) => i + 1).reverse();
  }

}
