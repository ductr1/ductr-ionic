export interface ImdbData {
  tconst: string,
  title: string,
  rating: number,
  startYear: number,
  endYear: number,
  description: string,
  posterLink: string,
  type: string,
  genres: string[],
  alternateTitles: string[]
}