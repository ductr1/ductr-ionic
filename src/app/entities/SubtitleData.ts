export interface SubtitleData {
  tconst: string;
  filename: string;
  languageCode: string;
}
