import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genre } from 'src/app/entities/Genre';
import { Type } from 'src/app/entities/Type';
import { VideoFile } from 'src/app/entities/VideoFile';

@Injectable({
  providedIn: 'root',
})
export class ImdbSearchService {
  constructor(private httpClient: HttpClient) {}

  getTypes(): Observable<Type[]> {
    return this.httpClient.get<Type[]>('http://localhost:8085/types');
  }

  getGenres(): Observable<Genre[]> {
    return this.httpClient.get<Genre[]>('http://localhost:8085/genres');
  }

  search(searchParams: {
    name: string;
    rating?: number;
    type?: string;
    genre?: string;
    year?: number;
    page: number;
  }): VideoFile {
    return null;
  }
}
