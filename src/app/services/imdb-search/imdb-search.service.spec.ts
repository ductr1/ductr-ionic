import { TestBed } from '@angular/core/testing';

import { ImdbSearchService } from './imdb-search.service';

describe('ImdbSearchService', () => {
  let service: ImdbSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImdbSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
