import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ImdbData } from 'src/app/entities/ImdbData';
import { VideoFile } from '../../entities/VideoFile';

@Component({
  selector: 'app-image-video-card',
  templateUrl: './image-video-card.component.html',
  styleUrls: ['./image-video-card.component.scss'],
})
export class ImageVideoCardComponent implements OnInit {
  
  @Input() data: ImdbData | VideoFile;
  @Output() emitId: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  emitClick() {
    this.emitId.emit(this.data.tconst);
  }
}
